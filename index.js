const express = require("express")
const app = express()

app.use("/", (req, res) => res.json({ message : "Working fine" }))

app.listen(process.env.PORT || 3001, () => console.log("Server running in port"));
